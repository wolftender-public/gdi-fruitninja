#include "window.hpp"

const wchar_t Window::szClassName [] = L"MYWNDCLASS";

bool Window::isClassRegistered (HINSTANCE hInstance, LPCWSTR lpClassName) {
	WNDCLASSEX wcex;
	return GetClassInfoExW (hInstance, lpClassName, &wcex);
}

bool Window::registerClass (HINSTANCE hInstance, LPCWSTR lpClassName) {
	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof (WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = Window::wndProc_s;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon (NULL, IDI_APPLICATION);
	wcex.hCursor = LoadCursor (nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = lpClassName;
	wcex.hIconSm = LoadIcon (NULL, IDI_APPLICATION);

	return RegisterClassExW (&wcex);
}

LRESULT Window::wndProc_s (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	Window * w = nullptr;

	if (uMsg == WM_NCCREATE) {
		CREATESTRUCT * pcs = (CREATESTRUCT *) lParam;
		w = (Window *) pcs->lpCreateParams;

		SetWindowLongPtr (hWnd, GWLP_USERDATA, (LONG_PTR) w);
		w->m_hWnd = hWnd;
	} else {
		w = (Window *) GetWindowLongPtr (hWnd, GWLP_USERDATA);
	}

	if (w) {
		auto r = w->wndProc (hWnd, uMsg, wParam, lParam);

		if (uMsg == WM_NCDESTROY) {
			w->m_hWnd = nullptr;
			SetWindowLongPtr (hWnd, GWLP_USERDATA, 0);
		}

		return r;
	}

	return DefWindowProc (hWnd, uMsg, wParam, lParam);
}

HWND Window::getWindowHandle () {
	return m_hWnd;
}

HINSTANCE Window::getInstanceHandle () {
	return m_hInstance;
}

bool Window::isValid () {
	if (m_hWnd) {
		return IsWindow (m_hWnd);
	}

	return false;
}

LRESULT Window::wndProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (uMsg == WM_CLOSE) {
		DestroyWindow (hWnd);
	}

	return DefWindowProc (hWnd, uMsg, wParam, lParam);
}

Window::Window (HINSTANCE hInstance, LPCWSTR lpClassName, Window * parent, const std::wstring & title, int x, int y, int width, int height, DWORD dwStyle, DWORD dwStyleEx) :
	m_hInstance (hInstance),
	m_hWnd (nullptr) {

	if (!lpClassName) {
		m_lpClassName = Window::szClassName;
	} else {
		m_lpClassName = lpClassName;
	}

	if (!Window::isClassRegistered (hInstance, m_lpClassName)) {
		if (!Window::registerClass (hInstance, m_lpClassName)) {
			DWORD dwError = GetLastError ();
			std::wstring message = L"Failed to create window class, Error Code " + std::to_wstring (dwError);

			MessageBox (NULL, message.c_str (), title.c_str (), MB_OK | MB_ICONERROR);
			return;
		}
	}

	HWND hWndParent = nullptr;
	if (parent) {
		if (parent->isValid ()) hWndParent = parent->getWindowHandle ();
	}

	HWND hWnd = CreateWindowEx (dwStyleEx, m_lpClassName, title.c_str (), dwStyle, x, y, width, height, hWndParent, nullptr, m_hInstance, (LPVOID) this);
}

Window::~Window () {
	if (isValid ()) {
		DestroyWindow (m_hWnd);
	}
}
