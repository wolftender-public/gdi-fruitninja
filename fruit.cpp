#include "fruit.hpp"
#include "mainwindow.hpp"

#include <cmath>

const int Fruit::deathBarrier = 100;

const Vector2 & Fruit::getVelocity () {
	return m_velocity;
}

const Vector2 & Fruit::getPosition () {
	return m_position;
}

void Fruit::setVelocity (const Vector2 & velocity) {
	m_velocity = velocity;
}

void Fruit::setPosition (const Vector2 & position) {
	m_position = position;
}

Fruit::Fruit (MainWindow * parent, Vector2 position, Vector2 velocity, int radius, HBRUSH & hbr) {
	m_parent = parent;
	m_position = position;
	m_velocity = velocity;
	m_radius = radius;
	m_brush = hbr;
	m_dead = false;

	m_invulnerability = 0.2f;

#ifdef _DEBUG
	OutputDebugString (L"spawned!\n");
#endif
}

Fruit::~Fruit () { 
#ifdef _DEBUG
	OutputDebugString (L"dead!\n");
#endif
}

void Fruit::update (float deltaTime) {
	m_position.x = m_position.x + deltaTime * m_velocity.x;
	m_position.y = m_position.y + deltaTime * m_velocity.y;

	if (m_invulnerability > 0) m_invulnerability -= deltaTime;

	// check if is dead
	if (m_position.x < -m_radius) m_dead = true;
	else if (m_position.x > m_parent->getScreenWidth () + m_radius) m_dead = true;
	else if (m_position.y > m_parent->getScreenHeight () + m_radius + Fruit::deathBarrier) m_dead = true;
}

void Fruit::render (HDC deviceContext) {
	if (!m_dead) {
		int x = round (m_position.x);
		int y = round (m_position.y);

		SelectObject (deviceContext, m_brush);
		Ellipse (deviceContext, x - m_radius, y - m_radius, x + m_radius, y + m_radius);
	}
}

inline int rand_range (int min, int max) {
	if (max == min) return max;
	return (rand () % (max - min)) + min;
}

inline Vector2 randomize_velocity (Vector2 parentVelocity, int sgn1, int sgn2, int dx, int dy) {
	static const float dv = 40.f;
	static const int range = 20;

	return Vector2 (
		parentVelocity.x + sgn1 * (rand_range (-range, range) + dv) + dx * 5,
		parentVelocity.y + sgn2 * (rand_range (-range, range) + dv) + dy * 5
	);
}

// the fruits deny laws of physics as one fruit can be divided into more than 1 worth of itself
// but it looks so much cooler this way
inline int randomize_radius (int radius) {
	return rand_range (radius / 2, 2 * radius / 3);
}

void Fruit::onMouseMove (int mouseX, int mouseY, int dx, int dy) {
	if (!m_dead && m_radius > 4 && m_invulnerability <= 0) {
		float dx = m_position.x - mouseX;
		float dy = m_position.y - mouseY;

		float dist = sqrtf (dx * dx + dy * dy);
		
		if (dist <= m_radius) {
			float diff = (float) m_radius / 2;

			m_parent->spawnFruit (Vector2 (m_position.x - diff, m_position.y - diff), randomize_velocity (m_velocity, -1, -1, dx, dy), randomize_radius (m_radius), m_brush);
			m_parent->spawnFruit (Vector2 (m_position.x + diff, m_position.y - diff), randomize_velocity (m_velocity, 1, -1, dx, dy), randomize_radius (m_radius), m_brush);
			m_parent->spawnFruit (Vector2 (m_position.x - diff, m_position.y + diff), randomize_velocity (m_velocity, -1, 1, dx, dy), randomize_radius (m_radius), m_brush);
			m_parent->spawnFruit (Vector2 (m_position.x + diff, m_position.y + diff), randomize_velocity (m_velocity, 1, 1, dx, dy), randomize_radius (m_radius), m_brush);

			m_parent->addPoints (1);
			m_dead = true;
		}
	}
}

bool Fruit::isDead () {
	return m_dead;
}
