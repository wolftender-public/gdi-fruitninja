#include "mainwindow.hpp"
#include "resource.h"

#include <windowsx.h>

#pragma comment(lib, "Msimg32.lib")

// makes debugging easier
// #define NO_TOPMOST

// constants
#ifdef NO_TOPMOST
const DWORD MainWindow::dwWindowStyleEx = WS_EX_LAYERED;
#else
const DWORD MainWindow::dwWindowStyleEx = WS_EX_TOPMOST | WS_EX_LAYERED;
#endif

const DWORD MainWindow::dwWindowStyle = (WS_OVERLAPPEDWINDOW & (~(WS_MAXIMIZE | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_THICKFRAME))) | WS_VISIBLE;
const int MainWindow::boardTileSize = 50; // px
const COLORREF MainWindow::fruitPalette [PALETTE_COLORS] = { RGB (237, 28, 35), RGB (255, 194, 14), RGB (255, 126, 0), RGB (0, 183, 239), RGB (111, 49, 152), RGB (168, 230, 29) };
const int MainWindow::framerate = 35;
const int MainWindow::mainTimerId = 123;
const float MainWindow::gameDuration = 30.0f;
const int MainWindow::progressBarHeight = 32;

MainWindow::MainWindow (HINSTANCE hInstance, int boardWidth, int boardHeight) 
	: Window (hInstance, L"FruitNinja", CW_USEDEFAULT, CW_USEDEFAULT,
	100, 100, MainWindow::dwWindowStyle, MainWindow::dwWindowStyleEx) {

	m_boardWidth = boardWidth;
	m_boardHeight = boardHeight;
	m_hKnifeCursor = NULL;

	m_hBackBuffer = NULL;
	m_hOldBackBuffer = NULL;
	m_hDeviceContext = NULL;
	m_hChessBrush [0] = m_hChessBrush [1] = NULL;

	m_running = false;
	m_startOnKeypress = false;

	m_isInactive = false;
	m_inactiveTimer = 0.0f;
	m_isMouseInside = false;

	m_lastUsedColor = 0;
	for (int i = 0; i < PALETTE_COLORS; ++i) {
		m_hFruitBrushes [i] = CreateSolidBrush (MainWindow::fruitPalette [i]);
	}

	m_lastMouseX = m_lastMouseY = 0;
	m_spawnTimer = 0.0f;
	m_hMainFont = NULL;
	m_hWhitePen = NULL;

	m_lastClock = clock ();
	m_lastRecordedMouse = clock ();

	m_mainTimer = 0.0f;
	m_score = 0;

	m_hOverlayDC = NULL;
	m_hOverlayBitmap = NULL;
	m_hOverlayBrush = NULL;

	if (isValid ()) {
		SetLayeredWindowAttributes (getWindowHandle (), 0, 255, LWA_ALPHA);
		adjustWindowPosition ();

		HMENU hMenu = LoadMenu (getInstanceHandle (), MAKEINTRESOURCE (IDR_MENU_MAIN));
		SetMenu (getWindowHandle (), hMenu);

		HICON hIcon = LoadIcon (getInstanceHandle (), MAKEINTRESOURCE (IDI_FRUITNINJA));
		SendMessage (getWindowHandle (), WM_SETICON, ICON_SMALL, (WPARAM) hIcon);
		SendMessage (getWindowHandle (), WM_SETICON, ICON_BIG, (WPARAM) hIcon);

		m_hKnifeCursor = LoadCursor (getInstanceHandle (), MAKEINTRESOURCE (IDC_KNIFE));

		// initialize context and back buffer
		HDC hdc = GetDC (getWindowHandle ());
		m_hDeviceContext = CreateCompatibleDC (hdc);
		ReleaseDC (getWindowHandle (), hdc);

		// use some nicer colors that give less contrast to put less strain on players eyes
		// black and white give so much contrast its painful sometimes :/
		m_hChessBrush [0] = CreateSolidBrush (RGB (65, 65, 65));
		m_hChessBrush [1] = CreateSolidBrush (RGB (210, 210, 210));

		// create font
		m_hMainFont = CreateFont (-MulDiv (20, GetDeviceCaps (m_hDeviceContext, LOGPIXELSY), 72), 0, 0, 0, FW_BOLD, false, false, false, 
			EASTEUROPE_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");

		m_hWhitePen = CreatePen (PS_SOLID, 3, RGB (255, 255, 255));

		m_hProgressBackground = CreateSolidBrush (RGB (30, 112, 52));
		m_hProgressForeground = CreateSolidBrush (RGB (38, 212, 84));

		updateBackBuffer ();
		UpdateWindow (getWindowHandle ());

		const int interval = 1000 / MainWindow::framerate;
		SetTimer (getWindowHandle (), MainWindow::mainTimerId, interval, NULL);

		startNewGame (boardWidth, boardHeight);
	}
}

MainWindow::~MainWindow () {
	if (m_hOldBackBuffer != NULL) SelectObject (m_hDeviceContext, m_hOldBackBuffer);
	if (m_hDeviceContext != NULL) DeleteDC (m_hDeviceContext);
	if (m_hBackBuffer != NULL) DeleteObject (m_hBackBuffer);

	if (m_hChessBrush [0]) DeleteObject (m_hChessBrush [0]);
	if (m_hChessBrush [1]) DeleteObject (m_hChessBrush [1]);
	if (m_hMainFont) DeleteObject (m_hMainFont);
	if (m_hWhitePen) DeleteObject (m_hWhitePen);

	if (m_hOldOverlayBitmap != NULL) SelectObject (m_hOverlayDC, m_hOldOverlayBitmap);
	if (m_hOverlayDC != NULL) DeleteDC (m_hOverlayDC);
	if (m_hOverlayBitmap) DeleteObject (m_hOverlayBitmap);

	if (m_hProgressBackground) DeleteObject (m_hProgressBackground);
	if (m_hProgressForeground) DeleteObject (m_hProgressForeground);

	for (int i = 0; i < PALETTE_COLORS; ++i) {
		DeleteObject (m_hFruitBrushes [i]);
	}

	KillTimer (getWindowHandle (), MainWindow::mainTimerId);
}

void MainWindow::spawnFruit (Vector2 position, Vector2 velocity, int size, HBRUSH hbr) {
	m_fruits.push_back (Fruit (this, position, velocity, size, hbr));
}

RECT MainWindow::calculateWindowRect () {
	RECT wr; // the preferred window rectangle
	wr.left = 0;
	wr.right = m_boardWidth * MainWindow::boardTileSize;
	
	wr.top = 0;
	wr.bottom = m_boardHeight * MainWindow::boardTileSize + MainWindow::progressBarHeight;

	// adjust the rect considering the frame of the window
	AdjustWindowRect (&wr, MainWindow::dwWindowStyle, TRUE);
	
	return wr;
}

inline UINT boolToCheck (bool val) {
	if (val) return MF_CHECKED;
	return MF_UNCHECKED;
}

void MainWindow::startNewGame (int boardWidth, int boardHeight) {
	m_boardWidth = boardWidth;
	m_boardHeight = boardHeight;
	m_score = 0;

	m_gameTimer = MainWindow::gameDuration;

	m_fruits.clear ();
	adjustWindowPosition ();

	m_running = false;
	m_startOnKeypress = true;

	// set the checkmark in the menu
	HMENU hMenu = GetMenu (getWindowHandle ());

	int gameType = 0;
	if (boardWidth == 8 && boardHeight == 6) gameType = 1;
	else if (boardWidth == 12 && boardHeight == 10) gameType = 2;
	else if (boardWidth == 16 && boardHeight == 12) gameType = 3;

	CheckMenuItem (hMenu, ID_NEWGAME_SMALLBOARD, boolToCheck (gameType == 1));
	CheckMenuItem (hMenu, ID_NEWGAME_MEDIUMBOARD, boolToCheck (gameType == 2));
	CheckMenuItem (hMenu, ID_NEWGAME_LARGEBOARD, boolToCheck (gameType == 3));
	CheckMenuItem (hMenu, ID_NEWGAME_CUSTOM, boolToCheck (gameType == 0));

	wchar_t buffer [4096];
	GetFullPathName (L"settings.ini", 4096, buffer, NULL);

	WritePrivateProfileString (L"Game", L"width", std::to_wstring (m_boardWidth).c_str (), buffer);
	WritePrivateProfileString (L"Game", L"height", std::to_wstring (m_boardHeight).c_str (), buffer);
}

void MainWindow::gameOver () {
	m_running = false;
}

void MainWindow::restoreFocus () {
	m_inactiveTimer = 0.0f;
	if (m_isInactive) {
		SetLayeredWindowAttributes (getWindowHandle (), 0, 255, LWA_ALPHA);
		m_isInactive = false;
	}
}

void MainWindow::addPoints (int pts) {
	m_score = m_score + pts;
}

void MainWindow::spawnFruit (Vector2 position, Vector2 velocity, int size) {
	spawnFruit (position, velocity, size, m_hFruitBrushes [m_lastUsedColor]);
	m_lastUsedColor = (m_lastUsedColor + 1) % PALETTE_COLORS;
}

void MainWindow::calculateWindowPosition (const RECT & wr, int & width, int & height, int & x, int & y) {
	RECT dr;
	GetClientRect (GetDesktopWindow (), &dr);

	int screenWidth = dr.right - dr.left;
	int screenHeight = dr.bottom - dr.top;

	width = wr.right - wr.left;
	height = wr.bottom - wr.top;

	x = (screenWidth - width) / 2;
	y = (screenHeight - height) / 2;
}

void MainWindow::adjustWindowPosition () {
	if (isValid ()) {
		RECT wr = calculateWindowRect ();
		int x, y, width, height;

		calculateWindowPosition (wr, width, height, x, y);

#ifdef NO_TOPMOST
		SetWindowPos (getWindowHandle (), HWND_TOP, x, y, width, height, NULL);
#else
		SetWindowPos (getWindowHandle (), HWND_TOPMOST, x, y, width, height, NULL);
#endif
	}
}

void MainWindow::updateBackBuffer () {
	int clientWidth = m_boardWidth * MainWindow::boardTileSize;
	int clientHeight = m_boardHeight * MainWindow::boardTileSize + MainWindow::progressBarHeight;

	HDC hdc = GetDC (getWindowHandle ());

	if (m_hOldBackBuffer != NULL) SelectObject (m_hDeviceContext, m_hOldBackBuffer);
	if (m_hBackBuffer != NULL) DeleteObject (m_hBackBuffer);

	m_hBackBuffer = CreateCompatibleBitmap (hdc, clientWidth, clientHeight); // use hdc because our mem dc has 1bpp bitmap
	m_hOldBackBuffer = (HBITMAP) SelectObject (m_hDeviceContext, m_hBackBuffer);

	// (i acually used the mem dc and was wondering why the color had one bit per pixel)
	// (but the effect was cool at least)

	// along with back buffer we update the third buffer - alpha overlay buffer
	if (m_hOldOverlayBitmap != NULL) SelectObject (m_hOverlayDC, m_hOldOverlayBitmap);
	if (m_hOverlayBitmap != NULL) DeleteObject (m_hOverlayBitmap);
	if (m_hOverlayDC != NULL) DeleteObject (m_hOverlayDC);

	m_hOverlayDC = CreateCompatibleDC (hdc);
	m_hOverlayBitmap = CreateCompatibleBitmap (hdc, clientWidth, clientHeight);

	m_hOldOverlayBitmap = (HBITMAP) SelectObject (m_hOverlayDC, m_hOverlayBitmap);
	RECT hFillRect = { 0, 0, clientWidth, clientHeight };
	FillRect (m_hOverlayDC, &hFillRect, m_hFruitBrushes [5]);

	ReleaseDC (getWindowHandle (), hdc);
}

inline int rand_range (int min, int max) {
	if (max == min) return max;
	return (rand () % (max - min)) + min;
}

// useful formula:
// v = sqrt (2g * h)
// v - initial velocity needed to reach
// h - maximum height when acceleration is g
void MainWindow::onUpdate (float deltaTime) {
	static const float spawnTimer = 1.30f;
	static const int spawnLimit = 150;
	static const int gravity = 180;
	static const float magicNumber = sqrtf (1.5f); // the lower the higher they go

	m_mainTimer += deltaTime;
	m_inactiveTimer += deltaTime;

	if (!m_isInactive && !m_isMouseInside && m_inactiveTimer > 3.0f) {
		SetLayeredWindowAttributes (getWindowHandle (), 0, (255 * 50) / 100, LWA_ALPHA);
		m_isInactive = true;
	}

	if (m_running) {
		float realSpawnTimer = spawnTimer - (m_boardHeight * m_boardWidth) * 0.008f;
		if (realSpawnTimer < 0.5f) realSpawnTimer = 0.5f;

		m_spawnTimer += deltaTime;
		if (m_fruits.size () < spawnLimit) {
			if (m_spawnTimer > realSpawnTimer) {
				int m_radius = rand_range (15 + m_boardWidth, 25 + m_boardWidth);

				int x = rand_range (m_radius, getScreenWidth () - m_radius);
				int y = getScreenHeight () + m_radius;

				int v_y_max = sqrtf (2 * gravity * (getScreenHeight () + m_radius));
				int v_y = rand_range (v_y_max / magicNumber, v_y_max);

				int v_x_max = (80 * (getScreenWidth () - abs (x))) / getScreenWidth (); // magic formula :)
				int v_x = rand_range (-v_x_max, v_x_max);

				spawnFruit (Vector2 (x, y), Vector2 (v_x, -v_y), m_radius);
				m_spawnTimer = m_spawnTimer - realSpawnTimer;
			}
		}
		
		if (m_gameTimer > 0.0f) {
			m_gameTimer -= deltaTime;
		} else {
			gameOver ();
		}

		for (std::list <Fruit>::iterator it = m_fruits.begin (); it != m_fruits.end (); ++it) {
			it->update (deltaTime);

			// apply gravity
			Vector2 velocity = it->getVelocity ();
			velocity.y += gravity * deltaTime;
			it->setVelocity (velocity);

			if (it->isDead ()) {
				it = m_fruits.erase (it);
				if (it == m_fruits.begin ()) break;
				it--;
			}
		}
	}

	if (m_mouseLinePoints.size () > 0) m_mouseLinePoints.pop_back ();
}

int MainWindow::getScreenWidth () {
	return m_boardWidth * MainWindow::boardTileSize;
}

int MainWindow::getScreenHeight () {
	return m_boardHeight * MainWindow::boardTileSize;
}

void MainWindow::onDraw (WPARAM wParam, LPARAM lParam) {
	// update before draw
	clock_t current = clock ();
	clock_t elapsed = current - m_lastClock;

	float deltaTime = (float) elapsed / CLOCKS_PER_SEC;
	onUpdate (deltaTime);
	m_lastClock = current;

	// ok, now we can draw
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint (getWindowHandle (), &ps);

	RECT cr;
	GetClientRect (getWindowHandle (), &cr);

	int clientWidth = cr.right - cr.left;
	int clientHeight = cr.bottom - cr.top;

	// keep old brush
	HBRUSH hOldBrush = (HBRUSH) SelectObject (m_hDeviceContext, (HBRUSH) GetStockObject (BLACK_BRUSH));
	RECT curr;

	// draw the background
	for (int i = 0; i < m_boardWidth * m_boardHeight; ++i) {
		int x = i % m_boardWidth;
		int y = i / m_boardWidth;

		int cx = x * MainWindow::boardTileSize;
		int cy = y * MainWindow::boardTileSize;

		curr.left = cx;
		curr.right = cx + MainWindow::boardTileSize;
		curr.top = cy;
		curr.bottom = cy + MainWindow::boardTileSize;

		FillRect (m_hDeviceContext, &curr, m_hChessBrush [(i + (y % 2)) % 2]);
	}

	// paint fruits
	for (std::list <Fruit>::iterator it = m_fruits.begin (); it != m_fruits.end (); ++it) {
		it->render (m_hDeviceContext);
	}

	// draw transparent on gameover
	if (!m_running && !m_startOnKeypress) {
		BLENDFUNCTION bf = {0, 0, 128, 0};
		AlphaBlend (m_hDeviceContext, 0, 0, clientWidth, clientHeight, m_hOverlayDC, 0, 0, clientWidth, clientHeight, bf);
	}

	// draw text
	bool visible = (int)(m_mainTimer / 0.5f) % 2;
	RECT sr;
	GetClientRect (getWindowHandle (), &sr);

	HFONT hPrevFont = (HFONT) SelectObject (m_hDeviceContext, m_hMainFont);

	if (!m_running && m_startOnKeypress && m_hMainFont && visible) {
		SetTextColor (m_hDeviceContext, RGB (255, 69, 0));

		int prevMode = SetBkMode (m_hDeviceContext, TRANSPARENT);
		DrawText (m_hDeviceContext, L"Click anywhere to start!", -1, &sr, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		SetBkMode (m_hDeviceContext, prevMode);
	}

	SetTextColor (m_hDeviceContext, RGB (255, 69, 0));
	COLORREF prevColor = SetBkColor (m_hDeviceContext, RGB (32, 32, 32));

	std::wstring scoreString = L" " + std::to_wstring (m_score);

	DrawText (m_hDeviceContext, scoreString.c_str (), -1, &sr, DT_RIGHT | DT_TOP | DT_SINGLELINE);
	SetBkColor (m_hDeviceContext, prevColor);

	// game over text
	if (!m_running && !m_startOnKeypress) {
		std::wstring gameOverString = L"Game over, score: " + std::to_wstring (m_score);
		SetTextColor (m_hDeviceContext, RGB (255, 0, 0));

		int prevMode = SetBkMode (m_hDeviceContext, TRANSPARENT);

		sr.top = getScreenHeight () / 2 - 32;
		DrawText (m_hDeviceContext, gameOverString.c_str (), -1, &sr, DT_CENTER);

		if (visible) {
			sr.top = getScreenHeight () / 2 + 16;
			DrawText (m_hDeviceContext, L"Press spacebar to restart", -1, &sr, DT_CENTER);
		}
	}

	SelectObject (m_hDeviceContext, hPrevFont);

	// draw polyline
	if (m_running) {
		POINT lastPos, startPos;

		GetCursorPos (&lastPos);
		ScreenToClient (getWindowHandle (), &lastPos);

		MoveToEx (m_hDeviceContext, lastPos.x, lastPos.y, &startPos);
		HPEN hPrevPen = (HPEN) SelectObject (m_hDeviceContext, m_hWhitePen);

		for (std::list <POINT>::iterator it = m_mouseLinePoints.begin (); it != m_mouseLinePoints.end (); ++it) {
			LineTo (m_hDeviceContext, it->x, it->y);
		}

		SelectObject (m_hDeviceContext, hPrevPen);
		MoveToEx (m_hDeviceContext, startPos.x, startPos.y, NULL);
	}

	// render the progress bar
	RECT pbr;
	pbr.left = 0;
	pbr.top = getScreenHeight ();
	pbr.right = getScreenWidth ();
	pbr.bottom = getScreenHeight () + MainWindow::progressBarHeight;

	RECT fgr;
	fgr.left = pbr.left + 3;
	fgr.top = pbr.top + 3;
	fgr.bottom = pbr.bottom - 3;
	fgr.right = fgr.left + (pbr.right - pbr.left - 6) * ((gameDuration - m_gameTimer) / gameDuration);

	FillRect (m_hDeviceContext, &pbr, m_hProgressBackground);
	FillRect (m_hDeviceContext, &fgr, m_hProgressForeground);
	
	// after painting go back to old brush
	SelectObject (m_hDeviceContext, hOldBrush);

	// swap buffer
	BitBlt (hdc, 0, 0, clientWidth, clientHeight, m_hDeviceContext, 0, 0, SRCCOPY);
	EndPaint (getWindowHandle (), &ps);
}

struct CustomGameData {
	int width, height;

	CustomGameData () : width (8), height (6) { }
	CustomGameData (int w, int h) : width (w), height (h) { }
};

BOOL CALLBACK CustomGameDialogProc (HWND hWndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
		case WM_INITDIALOG:
		{
			CustomGameData * cgd = (CustomGameData *) lParam;

			if (!cgd) {
				return EndDialog (hWndDlg, IDABORT);
			}

			SetDlgItemText (hWndDlg, IDC_EDIT_WIDTH, std::to_wstring (cgd->width).c_str ());
			SetDlgItemText (hWndDlg, IDC_EDIT_HEIGHT, std::to_wstring (cgd->height).c_str ());

			SendDlgItemMessage (hWndDlg, IDC_EDIT_WIDTH, EM_SETLIMITTEXT, 2, NULL);
			SendDlgItemMessage (hWndDlg, IDC_EDIT_HEIGHT, EM_SETLIMITTEXT, 2, NULL);

			SetWindowLongPtr (hWndDlg, GWLP_USERDATA, (LONG_PTR) cgd);
			break;
		}

		case WM_COMMAND:
		{
			switch (wParam) {
				case IDOK: 
				{
					// set the values in the struct
					CustomGameData * cgd = (CustomGameData *) GetWindowLongPtr (hWndDlg, GWLP_USERDATA);

					wchar_t bufferWidth [3], bufferHeight [3];
					GetDlgItemText (hWndDlg, IDC_EDIT_WIDTH, bufferWidth, 3);
					GetDlgItemText (hWndDlg, IDC_EDIT_HEIGHT, bufferHeight, 3);

					cgd->width = _wtoi (bufferWidth);
					cgd->height = _wtoi (bufferHeight);

					if (cgd->width > 16 || cgd->width < 8) {
						MessageBox (hWndDlg, L"Width has to be in range [8;16]", L"Error", MB_ICONERROR | MB_OK);
						return false;
					}

					if (cgd->height > 16 || cgd->height < 6) {
						MessageBox (hWndDlg, L"Width has to be in range [6;16]", L"Error", MB_ICONERROR | MB_OK);
						return false;
					}
					
					EndDialog (hWndDlg, IDOK);
					return true;
				}

				case (IDCANCEL): 
				{
					EndDialog (hWndDlg, IDCANCEL);
					return true;
				}
			}
		}
	}

	return false;
}

void MainWindow::onCommand (WPARAM wParam, LPARAM lParam) {
	int id = LOWORD (wParam);

	switch (id) {
		case ID_GAME_EXIT:
		{
			DestroyWindow (getWindowHandle ());
			break;
		}

		case ID_NEWGAME_SMALLBOARD:
		{
			startNewGame (8, 6);
			break;
		}

		case ID_NEWGAME_MEDIUMBOARD:
		{
			startNewGame (12, 10);
			break;
		}

		case ID_NEWGAME_LARGEBOARD:
		{
			startNewGame (16, 12);
			break;
		}

		case ID_NEWGAME_CUSTOM: // this is the fun one
		{
			CustomGameData cgd (m_boardWidth, m_boardHeight);
			UINT res = DialogBoxParam (getInstanceHandle (), MAKEINTRESOURCE (IDD_DIALOG_CUSTOM), 
				getWindowHandle (), CustomGameDialogProc, (LPARAM) &cgd);

			if (res == IDOK) {
				startNewGame (cgd.width, cgd.height);
			} else if (res == IDCANCEL) { 
				break;
			} else {
				MessageBox (getWindowHandle (), L"Error while processing dialog data", L"Error", MB_OK | MB_ICONERROR);
			}
			
			break;
		}
	}
}

LRESULT MainWindow::wndProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
		case WM_COMMAND:
		{
			onCommand (wParam, lParam);
			break;
		}

		case WM_TIMER:
		{
			//InvalidateRect (getWindowHandle (), NULL, false);
			RedrawWindow (getWindowHandle (), NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
			break;
		}

		case WM_ERASEBKGND: return true;

		case WM_SIZE: // resize the back buffer
		{
			updateBackBuffer ();
			InvalidateRect (getWindowHandle (), NULL, false);
			break;
		}

		case WM_PAINT: // handle window painting
		{
			onDraw (wParam, lParam);
			break;
		}

		case WM_LBUTTONDOWN:
		{
			if (m_startOnKeypress && !m_running) {
				m_running = true;
				m_startOnKeypress = false;
			}

			break;
		}

		case WM_KEYDOWN:
		{
			if (wParam == VK_SPACE) {
				if (!m_running && !m_startOnKeypress) {
					restoreFocus ();
					startNewGame (m_boardWidth, m_boardHeight);
				}
			}

			break;
		}

		case WM_MOUSELEAVE:
		{
			m_isMouseInside = false;
			break;
		}

		case WM_MOUSEMOVE:
		{
			if (!m_isMouseInside) {
				TRACKMOUSEEVENT tme;
				tme.cbSize = sizeof (TRACKMOUSEEVENT);
				tme.dwFlags = TME_HOVER | TME_LEAVE;
				tme.dwHoverTime = 1;
				tme.hwndTrack = getWindowHandle ();
				TrackMouseEvent (&tme);
			}

			m_isMouseInside = true;
			restoreFocus ();
			const int maxPoints = 10;

			// mouse moving logic here
			int mouseX = GET_X_LPARAM (lParam);
			int mouseY = GET_Y_LPARAM (lParam);

			int dx = mouseX - m_lastMouseX;
			int dy = mouseY - m_lastMouseY;

			if (m_running) {
				for (std::list <Fruit>::iterator it = m_fruits.begin (); it != m_fruits.end (); ++it)
					it->onMouseMove (mouseX, mouseY, dx, dy);
			}

			m_lastMouseX = mouseX;
			m_lastMouseY = mouseY;

			clock_t now = clock ();

			if (now - m_lastRecordedMouse > 10) {
				if (m_mouseLinePoints.size () > maxPoints) {
					m_mouseLinePoints.pop_back ();
				}

				POINT curr;
				curr.x = mouseX;
				curr.y = mouseY;

				m_mouseLinePoints.push_front (curr);

				m_lastRecordedMouse = now;
			}

			break;
		}

		// other events
		case WM_WINDOWPOSCHANGING: 
		{
			WINDOWPOS * wp = (WINDOWPOS *) lParam;
			calculateWindowPosition (calculateWindowRect (), wp->cx, wp->cy, wp->x, wp->y);

			return 0;
		}

		case WM_GETMINMAXINFO:
		{
			RECT r = calculateWindowRect ();

			MINMAXINFO * minMaxInfo = (MINMAXINFO *) lParam;
			minMaxInfo->ptMinTrackSize.x = minMaxInfo->ptMaxTrackSize.x = r.right - r.left;
			minMaxInfo->ptMinTrackSize.y = minMaxInfo->ptMaxTrackSize.y = r.bottom - r.top;

			break;
		}

		case WM_SETCURSOR:
		{
			if (LOWORD (lParam) == HTCLIENT) {
				SetCursor (m_hKnifeCursor);
				return true;
			}

			break;
		}

		// this is the main window so closing it = closing application!
		case WM_CLOSE: DestroyWindow (getWindowHandle ()); break;
		case WM_DESTROY: PostQuitMessage (0); break;
	}

	return Window::wndProc (hWnd, uMsg, wParam, lParam);
}
