#pragma once
#include "window.hpp"
#include "fruit.hpp"

#include <list>
#include <ctime>

// the class for main window, the game state is tied to this class, but it
// could be separated, it just isn't because the game is very simple

#define PALETTE_COLORS 6

class MainWindow : public Window {
	private:
		static const DWORD dwWindowStyle, dwWindowStyleEx;
		static const int boardTileSize;
		static const COLORREF fruitPalette [PALETTE_COLORS];
		static const int mainTimerId;
		static const int framerate;
		static const float gameDuration;
		static const int progressBarHeight;

	private:
		int m_boardWidth, m_boardHeight, m_lastUsedColor;
		clock_t m_lastClock, m_lastRecordedMouse;
		HCURSOR m_hKnifeCursor;

		// double buffering
		HDC m_hDeviceContext;
		HBITMAP m_hBackBuffer, m_hOldBackBuffer;
		
		// brushes
		HBRUSH m_hChessBrush [2];
		HBRUSH m_hFruitBrushes [PALETTE_COLORS];

		// alpha bitmap
		HBRUSH m_hOverlayBrush;
		HBITMAP m_hOverlayBitmap, m_hOldOverlayBitmap;
		HDC m_hOverlayDC;

		// fonts
		HFONT m_hMainFont;

		// pens
		HPEN m_hWhitePen;

		// fruits
		std::list <Fruit> m_fruits;
		float m_spawnTimer;

		int m_lastMouseX, m_lastMouseY;
		bool m_running, m_startOnKeypress;
		float m_mainTimer;

		// poly line
		std::list <POINT> m_mouseLinePoints;

		int m_score;
		float m_gameTimer;

		// transparency
		float m_inactiveTimer;
		bool m_isInactive, m_isMouseInside;

		// progress bar
		HBRUSH m_hProgressBackground, m_hProgressForeground;

	public:
		MainWindow (HINSTANCE hInstance, int boardWidth, int boardHeight);
		~MainWindow ();

		void spawnFruit (Vector2 position, Vector2 velocity, int size, HBRUSH hbr);
		void spawnFruit (Vector2 position, Vector2 velocity, int size);
		void addPoints (int pts);

		int getScreenWidth ();
		int getScreenHeight ();

	private:
		RECT calculateWindowRect ();
		void calculateWindowPosition (const RECT & wr, int & width, int & height, int & x, int & y);

		void adjustWindowPosition ();
		void updateBackBuffer ();

		void startNewGame (int boardWidth, int boardHeight);
		void gameOver ();
		void restoreFocus ();

		// event handler functions, decluttering the wndproc
		void onDraw (WPARAM wParam, LPARAM lParam);
		void onUpdate (float deltaTime);
		void onCommand (WPARAM wParam, LPARAM lParam);

	public:
		LRESULT CALLBACK wndProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) override;
};