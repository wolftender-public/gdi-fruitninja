#include <Windows.h>
#include <commdlg.h>
#include <shellapi.h>
#include <CommCtrl.h>

#include "resource.h"
#include "mainwindow.hpp"

#pragma comment (lib, "Comctl32.lib")

// modern styles for common controls
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

int WINAPI wWinMain (_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
	srand (time (NULL));

	HACCEL hAccelTable = LoadAccelerators (hInstance, MAKEINTRESOURCE (IDR_ACCELERATOR));

	wchar_t buffer [4096];
	GetFullPathName (L"settings.ini", 4096, buffer, NULL);

	int width = GetPrivateProfileInt (L"Game", L"width", 8, buffer);
	int height = GetPrivateProfileInt (L"Game", L"height", 6, buffer);

	if (width < 8) width = 8;
	else if (width > 16) width = 16;

	if (height < 6) height = 6;
	else if (height > 16) height = 16;

	MainWindow mainWindow (hInstance, width, height);

	if (!mainWindow.isValid ()) {
		MessageBox (NULL, L"Failed to create main window", L"Error", MB_ICONERROR | MB_OK);
		return 1;
	}

	MSG msg;

	// main message loop
	while (GetMessage (&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator (msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage (&msg);
			DispatchMessage (&msg);
		}
	}

	return 0;
}