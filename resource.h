//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by lab3.rc
//
#define IDR_MENU_MAIN                   101
#define IDR_ACCELERATOR                 102
#define IDI_FRUITNINJA                  103
#define IDC_KNIFE                       104
#define IDD_DIALOG1                     105
#define IDD_DIALOG_CUSTOM               105
#define IDC_EDIT_WIDTH                  1001
#define IDC_EDIT_HEIGHT                 1002
#define ID_GAME_STARTGAME               40001
#define ID_GAME_EXIT                    40002
#define ID_NEWGAME_SMALLBOARD           40003
#define ID_NEWGAME_MEDIUMBOARD          40004
#define ID_NEWGAME_LARGEBOARD           40005
#define ID_NEWGAME_CUSTOM               40006
#define ID_NEWGAME                      40007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40009
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
