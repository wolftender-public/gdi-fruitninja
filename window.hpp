#pragma once
#include <Windows.h>
#include <string>

class Window {
	private:
		static const wchar_t szClassName [];

	private:
		static bool isClassRegistered (HINSTANCE hInstance, LPCWSTR lpClassName);
		static bool registerClass (HINSTANCE hInstance, LPCWSTR lpClassName);
		static LRESULT CALLBACK wndProc_s (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	private:
		HINSTANCE m_hInstance;
		LPCWSTR m_lpClassName;
		HWND m_hWnd;

	public:
		HWND getWindowHandle ();
		HINSTANCE getInstanceHandle ();
		bool isValid ();

	public:
		virtual LRESULT CALLBACK wndProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
		
		// constructors
		Window (HINSTANCE hInstance, LPCWSTR lpClassName, Window * parent, const std::wstring & title, int x, int y, int width, int height, DWORD dwStyle, DWORD dwStyleEx);
		
		Window (HINSTANCE hInstance, Window * parent, const std::wstring & title, int x, int y, int width, int height, DWORD dwStyle, DWORD dwStyleEx) :
			Window (hInstance, nullptr, parent, title, x, y, width, height, dwStyle, dwStyleEx) { }
		Window (HINSTANCE hInstance, const std::wstring & title, int x, int y, int width, int height, DWORD dwStyle, DWORD dwStyleEx) :
			Window (hInstance, nullptr, nullptr, title, x, y, width, height, dwStyle, dwStyleEx) { }

		~Window ();

		// clutter
		Window (const Window & w) = delete; // copy constructor
		Window & operator= (const Window & w) = delete; // assignement operator
};