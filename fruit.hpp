#pragma once
#include <Windows.h>

// theres only one type of renderable, so there is no need for anything like an interface for renderables
struct Vector2 {
	float x, y;

	Vector2 () : x (0), y (0) { }
	Vector2 (float x, float y) : x (x), y (y) { }
};

class MainWindow;
class Fruit {
	private:
		static const int deathBarrier;

	private:
		MainWindow * m_parent;
		Vector2 m_position, m_velocity;
		int m_radius;
		bool m_dead;

		float m_invulnerability;

		HBRUSH m_brush;

	public:
		const Vector2 & getVelocity ();
		const Vector2 & getPosition ();

		void setVelocity (const Vector2 & velocity);
		void setPosition (const Vector2 & position);

	public:
		Fruit (MainWindow * parent, Vector2 position, Vector2 velocity, int radius, HBRUSH & hbr);
		~Fruit ();

		void update (float deltaTime);
		void render (HDC deviceContext);
		void onMouseMove (int mouseX, int mouseY, int dx, int dy);

		bool isDead ();
};